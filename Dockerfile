FROM openjdk:8-alpine
LABEL maintainer="Sergio Rivas Medina -> sergiorm96@gmail.com"

ADD ./target/ /app
EXPOSE 9005
WORKDIR /app
CMD java -jar bff-customer*