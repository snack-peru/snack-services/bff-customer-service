package com.snack.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ControllerSuborderDTO {
    private Long idSuborder;
    private String suborderdisplayId;
    private Double subordersubtotal;
    private Double suborderdeliveryPrice;
    private Double subordertotal;
    private Integer suborderquantity;
    private Integer suborderidStore;
    private Long suborderidCustomer;
    private Integer suborderidPaymentType;
    private Integer suborderidDeliveryType;
    private Integer suborderidSuborderStatus;
    private List<ControllerSuborderLineDTO> controllerSuborderLineDto;
}