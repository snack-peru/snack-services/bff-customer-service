package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder

public class ProductBrandDto_StoreXProductResponse {
    private Long idBrand;
    private String name;
}
