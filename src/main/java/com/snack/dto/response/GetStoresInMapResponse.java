package com.snack.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.snack.dto.Coordinates;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class GetStoresInMapResponse implements Comparable<GetStoresInMapResponse>{
    private Integer storeId;
    private String title;
    private String description;
    @JsonProperty("coordinate")
    private Coordinates coordinates;
    private String image;
    private float rating;
    private Integer reviews;
    private String status;
    private Double distance;
    private String deliveryType;

    @Override
    public int compareTo(GetStoresInMapResponse getStoresInMapResponse) {
        return this.getDistance().compareTo(getStoresInMapResponse.getDistance());
    }
}
