package com.snack.dto.response;

import com.snack.dto.SubCategoryDto;
import com.snack.dto.ProductDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SubcategoryResponse {
    private SubCategoryDto subcategory;
    private List<ProductDto> products;
}
