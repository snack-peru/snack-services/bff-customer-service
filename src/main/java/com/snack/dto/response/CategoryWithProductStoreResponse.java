package com.snack.dto.response;

import java.util.List;

import com.snack.dto.ProductDto_CategoryWithProductStoreResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CategoryWithProductStoreResponse {
    private Integer idProductCategory;
    private String name;
    private Integer ranking;
    private List<ProductDto_CategoryWithProductStoreResponse> product;
}
