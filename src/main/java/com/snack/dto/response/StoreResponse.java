package com.snack.dto.response;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.snack.dto.DeliveryTypeDTO;
import com.snack.dto.StatusStoreDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class StoreResponse {
    private Integer idStore;
    private String name;
    private String ruc;
    private String address;
    private Double latitude;
    private Double longitude;
    private float score;
    private Integer reviews;
    private String image;
    private String openTime;
    private String closedTime;
    private DeliveryTypeDTO deliveryType;
    private StatusStoreDTO statusStore;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    private Date creationDate;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    private Date modificationDate;
}
