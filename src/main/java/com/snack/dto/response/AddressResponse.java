package com.snack.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AddressResponse {
    private Long idAddress;
    private Double latitude;
    private Double longitude;
    private String description;
    private Boolean isDefault;
    private Integer status;
}
