package com.snack.dto.response;

import com.snack.dto.ProductDto_StoreXProductResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class StoreProfileXProductResponse {
    private Long idStoreXProduct;
    private Double price;
    private Integer status;
    private Integer idStore;
    private ProductDto_StoreXProductResponse product;

}
