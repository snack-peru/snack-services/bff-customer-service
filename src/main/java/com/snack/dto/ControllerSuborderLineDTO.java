package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ControllerSuborderLineDTO {
    private Long idSuborderLine;
    private Integer suborderlinequantityBought;
    private Double suborderlineunitPrice;
    private Integer suborderlinequantityAvailable;
    private Integer suborderlineidStore;
    private Long suborderlineidProduct;
    private Integer suborderlineidSuborderLineStatus;
}