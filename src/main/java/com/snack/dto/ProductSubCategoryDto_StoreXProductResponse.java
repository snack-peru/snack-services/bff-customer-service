package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder

public class ProductSubCategoryDto_StoreXProductResponse {

    private Integer idProductSubCategory;
    private String name;
    private ProductCategoryDto_StoreXProductResponse productCategory;
    
}
