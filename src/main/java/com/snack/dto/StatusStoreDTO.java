package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class StatusStoreDTO {
    private Integer idStatusStore;
    private String description;
    private String name; 
}
