package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder

public class ProductCategoryDto_StoreXProductResponse {
    private Integer idProductCategory;
    private String name;
    private Integer ranking;
    
}
