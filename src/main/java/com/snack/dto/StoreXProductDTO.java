package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StoreXProductDTO {
    private Long idStoreXProduct;
    private Double price;
    private Integer idStore;
    private ProductDto product;
}