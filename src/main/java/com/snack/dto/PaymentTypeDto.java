package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentTypeDto {
    private Integer idPaymentType;
    private String name;
    private String description;
    private String type;
}
