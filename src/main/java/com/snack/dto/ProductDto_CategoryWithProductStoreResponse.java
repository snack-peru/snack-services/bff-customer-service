package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder

public class ProductDto_CategoryWithProductStoreResponse {
    private Long idStoreXProduct;
    private Long idProduct; 
    private Double price;
    private String content;
    private String name;
    private Integer status;
    private String image;
    private String brand;
    private String packaging;
    private String description;

}
