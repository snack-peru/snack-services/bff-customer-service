package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder

public class ProductDto_StoreXProductResponse {
    private Long idProduct; 
    private String content;
    private String name;
    private Integer status;
    private String image;
    private String description;
    private String packaging;
    private ProductBrandDto_StoreXProductResponse brand;
    private ProductSubCategoryDto_StoreXProductResponse productSubCategory;
}
