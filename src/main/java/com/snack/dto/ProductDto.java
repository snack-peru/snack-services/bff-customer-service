package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Author: Daniel-Claudio
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private Long idProduct;
    private String name;
    private String description;
    private String image;
    private BrandDto brand; 
    private UnitOfMeasurementDto unitOfMeasurement;
}
