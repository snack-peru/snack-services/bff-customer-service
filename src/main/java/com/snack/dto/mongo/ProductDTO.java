package com.snack.dto.mongo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDTO {
    private Long productId;
    private String brand;
    private String name;
    private Integer quantity;
    private String measurement;
    private Double price; // productsAlone: Precio Aprox y en List<ProductDto> de Store: precio real

    //Comentarios:
    //BasketDto -> StoreDto ->ProductDTO (Productos con tienda)
    //BasketDto -> ProductDto (Productos sin tienda)
}