package com.snack.dto.mongo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class StoreDTO {
    private Integer storeId;
    private String name;
    private List<ProductDTO> products;
    private String deliverytype;
}