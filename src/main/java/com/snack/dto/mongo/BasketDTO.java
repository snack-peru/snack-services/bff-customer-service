package com.snack.dto.mongo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasketDTO {
    private String basketId;
    private Long customerId;
    private List<StoreDTO> stores;
    private List<ProductDTO> productsAlone;
}