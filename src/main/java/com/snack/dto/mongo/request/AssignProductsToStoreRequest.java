package com.snack.dto.mongo.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AssignProductsToStoreRequest {
    private List<Long> productsId;
    private Integer storeId;
    private String storeName;
}
