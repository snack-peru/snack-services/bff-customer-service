package com.snack.dto.mongo.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddProductRequest {
    private Long productId;
    private String brand;
    private String name;
    //TODO: Validar si usar Integer o Double
    private Integer quantity;
    private String measurement;
    private Integer storeId; // para validar si tiene tienda asignada
    private String storeName;
}
