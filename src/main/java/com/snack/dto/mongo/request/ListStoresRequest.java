package com.snack.dto.mongo.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ListStoresRequest {
    private Long idCustomer;
    private List<BasketItem> products;
    private String deliveryType;
    private Double distance;
    private Double longitude;
    private Double latitude;
    private Integer page;
    private Integer pageLen;
}
