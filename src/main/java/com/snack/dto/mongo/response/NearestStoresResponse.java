package com.snack.dto.mongo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NearestStoresResponse {
    private List<NearestStoresResponseElement> stores;
}
