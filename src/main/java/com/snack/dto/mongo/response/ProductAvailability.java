package com.snack.dto.mongo.response;

import com.snack.dto.BrandDto;
import com.snack.dto.UnitOfMeasurementDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductAvailability {
    private Long idProduct;
    private Boolean available;
    private Double price;
}
