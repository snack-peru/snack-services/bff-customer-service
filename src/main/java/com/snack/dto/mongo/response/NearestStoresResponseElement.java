package com.snack.dto.mongo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.snack.dto.StatusStoreDTO;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NearestStoresResponseElement {
    private Integer idStore;
    private String address;
    private String closedTime;
    private String image;
    private Double latitude;
    private Double longitude;
    private String name;
    private String openTime;
    private StatusStoreDTO statusStore;
    private String ruc;
    private Double score;
    private List<ProductAvailability> products = new ArrayList<ProductAvailability>();

    public void addProduct(ProductAvailability p) {
        this.products.add(p);
    }
}
