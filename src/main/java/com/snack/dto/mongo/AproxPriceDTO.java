package com.snack.dto.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AproxPriceDTO {
    private Long productId;
    private Double aproxPrice;
}