package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class DeliveryTypeDTO {
   private Integer idDeliveryType;
   private String description;
   private String name; 
}
