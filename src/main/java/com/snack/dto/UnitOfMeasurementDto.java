package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnitOfMeasurementDto {
    private Integer idUnitOfMeasurement;
    private String name;
    private Integer status;
     
}