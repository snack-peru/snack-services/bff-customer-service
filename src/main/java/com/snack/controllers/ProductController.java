package com.snack.controllers;

import java.util.List;

import com.snack.dto.CustomerDto;
import com.snack.dto.ProductDto;
import com.snack.services.ProductService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@CrossOrigin
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(value="/api/customer/product/newproducts")
    public List<ProductDto> getNewProducts(){
        return productService.getNewProducts();
    }

    @GetMapping(value="/api/customer/product/{idProduct}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener un producto por su id",
            notes = "Se pasa productId como parámetro",
            responseContainer = "Object",
            response = ProductDto.class)
    public ProductDto getProductById(@PathVariable("idProduct") Long idProduct){
        return productService.getProductById(idProduct);
    }

    @GetMapping(value="/api/customer/product/search/{searchkey}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Buscar productos por una cadena a buscar",
            notes = "Se hace una busqueda que los productos que cumplen con la cadena espcificada",
            responseContainer = "Object")
    public List<ProductDto> getProductByName(@PathVariable("searchkey") String searchkey){
        return productService.getProductByName(searchkey);
    }

    @GetMapping(value = "/api/customer/product/image/{name:.+}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Se obtiene la imagen de un producto",
            notes = "se pasa el nombre de la imagen como parámetro",
            responseContainer = "Object")
    public ResponseEntity<Resource> getProductImage(@PathVariable String name) {
        Resource image = productService.getProductImage(name);
        return ResponseEntity
            .ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + image.getFilename() +"\"")
            .body(image);
    }

    @GetMapping(value="/api/customer/productsbycategory/{categoryId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Se obtiene una lista de productos por una categoría",
            notes = "Se especifica una categoría para obtener la lista de productos",
            responseContainer = "Object")
    public List<ProductDto> getProductsByCategory(@PathVariable("categoryId") Integer categoryId){
        return productService.getProductsByCategory(categoryId);
    }
}