package com.snack.controllers;

import java.util.List;

import com.snack.dto.CategoryDto;
import com.snack.dto.mongo.AproxPriceDTO;
import com.snack.dto.response.SubcategoryResponse;
import com.snack.services.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;

@RestController
@CrossOrigin
public class CategoryController {
    
    @Autowired
    private CategoryService categoryService;

    //Author: Sergio
    @GetMapping(value = "/api/customer/categories")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener todas las categorías",
            notes = "",
            responseContainer = "Object")
    public List<CategoryDto> getAllProductsCategories() {
        return categoryService.getAllProductsCategories();
    }

    //Author: Sergio
    @GetMapping(value = "/api/customer/category/image/{name:.+}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener la imagen de una categoría",
            notes = "Se pasa el nombre de la imagen como parámetro",
            responseContainer = "Object")
    public ResponseEntity<Resource> getCategoryImage(@PathVariable String name) {
        Resource image = categoryService.getCategoryImage(name);
        return ResponseEntity
            .ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + image.getFilename() +"\"")
            .body(image);
    }

    @GetMapping(value = "/api/customer/categoriesByRanking")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener todas la categorías por ranking",
            notes = "Se usa en la pantalla home",
            responseContainer = "Object")
    public List<CategoryDto> getCategoriesByRanking() {
        return categoryService.getCategoriesByRanking();
    }

    //Get all subcategories with products of a category
    @GetMapping(value = "/api/customer/category/{categoryId}/subcategories")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE,
                    value = "Obtiene todos las subcategorias con sus productos correspondiente a la categoria solicitada",
                    notes = "Se requiere el ID Category",
                    responseContainer = "Object")
    public List<SubcategoryResponse> getSubcategoriesWithProductsByCategory(@PathVariable("categoryId") Integer categoryId){
        return categoryService.getSubcategoriesWithProductsByCategory(categoryId);
    }
}