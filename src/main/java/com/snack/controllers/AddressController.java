package com.snack.controllers;

import com.snack.dto.request.AddressRequest;
import com.snack.dto.response.AddressResponse;
import com.snack.dto.response.GetAllAddressResponse;
import com.snack.services.AddressService;
import com.snack.services.LongResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Collections;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/customer/{customerId}/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    // Author: Sergio
    @GetMapping
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener las direcciones por un customerId",
            notes = "Solo se debe enviar como parámetro el customerId",
            responseContainer = "Object",
            response = GetAllAddressResponse.class)
    public List<AddressResponse> getAllAddressByCustomerId(@PathVariable("customerId") Long customerId) {
        return addressService.getAllAddressByCustomerId(customerId);
    }

    // Author: Sergio
    @GetMapping("/default")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener la dirección por defecto de un customerId",
            notes = "Se evnía como parámetro el customerId",
            responseContainer = "Object",
            response = AddressResponse.class)
    public ResponseEntity<?> getDefaultAddressByCustomerId(@PathVariable("customerId") Long customerId) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(addressService.getDefaultAddressByCustomerId(customerId));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Agregar dirección");
        }
    }

    // Author: Sergio
    @PostMapping
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Insertar una nueva dirección para un cliente",
            notes = "Se envía como parámetro el idCustomer y la nueva dirección",
            responseContainer = "Object",
            response = AddressResponse.class)
    public AddressResponse saveCustomerAddress(@PathVariable("customerId") Long customerId,
                                               @RequestBody AddressRequest addressRequest) {
        return addressService.saveCustomerAddress(customerId, addressRequest);
    }

    // Author: Sergio
    @DeleteMapping(value = "/{addressId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Se eliminar una dirección asociado a un cliente",
            notes = "Se debe enviar como parámetro el id de cleinte y el id de la dirección",
            responseContainer = "Object",
            response = GetAllAddressResponse.class)
    public ResponseEntity<?> deleteCustomerAddress(@PathVariable("customerId") Long customerId,
                                                   @PathVariable("addressId") Long addressId) {
        try {
            addressService.deleteCustomerAddress(addressId);
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }

        List<AddressResponse> addressResponseList = addressService.getAllAddressByCustomerId(customerId);
        return ResponseEntity.status(HttpStatus.OK).body(addressResponseList);
    }

    // Author: Sergio
    @PutMapping(value = "/{addressId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualizar la dirección por defecto",
            notes = "Se debe enviar como parámetro el id customer y el id de la dirección que será la nueva por defecto",
            responseContainer = "Object",
            response = LongResponse.class)
    public LongResponse updateDefaultAddress(@PathVariable("customerId") Long customerId,
                                             @PathVariable("addressId") Long newDefaultAddressId) {
        return addressService.updateDefaultAddress(customerId, newDefaultAddressId);
    }
}
