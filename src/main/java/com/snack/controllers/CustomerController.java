package com.snack.controllers;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snack.dto.*;
import com.snack.dto.mongo.AproxPriceDTO;
import com.snack.dto.request.GetStoresInMapRequest;
import com.snack.dto.response.GetStoresInMapResponse;
import com.snack.services.CustomerService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@CrossOrigin
@RequestMapping("/api/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping(value = "/{customerId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener un customer por su id",
            notes = "",
            responseContainer = "Object",
            response = CustomerDto.class)
    public CustomerDto getCustomerById(@PathVariable(name = "customerId") Long customerId) {
        return customerService.getCustomerById(customerId);
    }

    @PostMapping(value = "/login")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Login de customer",
            notes = "Se pasa como requestBody el usuario y contraseña",
            responseContainer = "Object")
    public ResponseEntity<?> loginCustomer(@RequestBody LoginDto loginDto) {
        try {
            CustomerLogged customer = customerService.loginCustomer(loginDto);
            return new ResponseEntity<CustomerLogged>(customer, HttpStatus.OK);
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }
    }

    @PostMapping(value = "/register")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Registro con email de un nuevo customer",
            notes = "Se pasan los datos de customer",
            responseContainer = "Object")
    public ResponseEntity<?> registerCustomer(@RequestBody RegisterUser newUser) {
        try {
            CustomerLogged customer = customerService.registerCustomer(newUser);
            return new ResponseEntity<CustomerLogged>(customer, HttpStatus.CREATED);
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }
    }

    @PostMapping(value = "/update")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Se actualizan los datos de customer",
            responseContainer = "Object")
    public ResponseEntity<?> updateCustomer(@RequestBody UserDto user) {
        try {
            UserDto updatedUser = customerService.updateCustomer(user);
            return new ResponseEntity<UserDto>(updatedUser, HttpStatus.OK);
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }
    }

    @PostMapping(value = "/{latitude}/{longitude}/stores/{radiusDistance}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener las tiendas cercanas a tu ubicación en el mapa",
            notes = "Se pasa un objeto con la lista de storesIds que ya se tienen para que la respuesta no las repita",
            responseContainer = "Object")
    public ResponseEntity<?> getStoresInMap(@PathVariable(name = "latitude") Double latitude,
                                           @PathVariable(name = "longitude") Double longitude,
                                           @PathVariable(name = "radiusDistance") Double radiusDistance,
                                           @RequestBody GetStoresInMapRequest request) {

        if (latitude == null || longitude == null || latitude.isNaN() || longitude.isNaN()) {
            // se devuelve un error porque no se ha podido conseguir las coordenadas
            Map<String, String> errors = new HashMap<>();
            errors.put("message", "Coordenadas no válidas");
            errors.put("error", "Latitude and longitude should be different of null and should be Doubles");
            errors.put("frontendMessage", "Active la localización de en su dispositivo");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(errors);
        }
        return ResponseEntity.status(HttpStatus.OK).body(customerService.getStoresInMap(latitude, longitude, request,
                radiusDistance));
    }
}