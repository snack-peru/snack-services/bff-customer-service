package com.snack.controllers;

import java.util.List;

import com.snack.dto.PaymentTypeDto;
import com.snack.dto.response.StoreResponse;
import com.snack.services.StoreService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@RequestMapping("/api/customer/store")
public class StoreController {

    @Autowired
    private StoreService storeService;

    @GetMapping(value="/search/{searchkey}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene las tiendas que coincida con la busqueda",
    notes = "Solo se debe enviar como parámetro un string",
    responseContainer = "Object")
    public List<StoreResponse> getStoresByName(@PathVariable("searchkey") String searchkey){
        return storeService.getStoresByName(searchkey);
    }

    @GetMapping(value="/get-payment-types/{idStore}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene los mètodos de pagos disponibles para una tienda",
            notes = "Solo se debe enviar como parámetro el Id de la tienda",
            responseContainer = "Object")
    public List<PaymentTypeDto> getPaymentTypes(@PathVariable("idStore") Long idStore){
        return storeService.getPaymentTypes(idStore);
    }
    
}
