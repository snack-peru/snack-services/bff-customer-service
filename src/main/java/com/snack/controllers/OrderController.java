package com.snack.controllers;

import java.util.Collections;
import java.util.List;

import com.snack.dto.CustomerDto;
import com.snack.services.OrderService;
import com.snack.dto.OrderDto;
import com.snack.dto.RegisterTotalOrderDto;


import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@CrossOrigin
@RequestMapping("/api/customer/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping(value = "/{idCustomer}/{idOrderStatus}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener todas las ordenes por customerId y status",
            notes = "Se pasa como parámetro el customerId y el status de las órdenes a buscar",
            responseContainer = "Object")
    public List<OrderDto> getAllOrdersByIdCustomerIdOrderStatus(@PathVariable("idCustomer") Long idCustomer,
            @PathVariable("idOrderStatus") Integer idOrderStatus) {
        return orderService.getAllOrdersByIdCustomerIdOrderStatus(idCustomer, idOrderStatus);
    }

    @PostMapping(value = "/{idCustomer}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Guardar una nueva orden",
            notes = "Se pasa como parámetro el customerId",
            responseContainer = "Object")
    public ResponseEntity<?> saveOrder (@PathVariable ("idCustomer") Long idCustomer, @RequestBody RegisterTotalOrderDto registerTotalOrderDto){

        try {
            RegisterTotalOrderDto OrderRegistered=orderService.saveTotalOrder(idCustomer,registerTotalOrderDto);
           
            return new ResponseEntity<RegisterTotalOrderDto>(OrderRegistered, HttpStatus.CREATED);
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }
    }


}