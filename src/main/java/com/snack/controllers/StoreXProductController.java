package com.snack.controllers;

import java.util.ArrayList;
import java.util.List;

import com.snack.dto.ProductDto_CategoryWithProductStoreResponse;
import com.snack.dto.response.CategoryWithProductStoreResponse;
import com.snack.dto.response.StoreProfileXProductResponse;
import com.snack.dto.response.StoreResponse;
import com.snack.services.StoreService;
import com.snack.services.StoreXProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@RequestMapping("/api/customer/storexproduct")
public class StoreXProductController {
    
    @Autowired
    private StoreXProductService storeXProductService;

    @GetMapping(value="/getProfileProducts/{idStore}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene los productos de la tienda que se mostraran en el perfil",
    notes = "Solo se debe enviar como parámetro el id de la tienda",
    responseContainer = "Object")
    public List<CategoryWithProductStoreResponse> getProfileStoreProducts(@PathVariable("idStore") Integer idStore){
        List<StoreProfileXProductResponse> storeProfileXProductResponse =  storeXProductService.getProfileStoreProducts(idStore);
        List<CategoryWithProductStoreResponse> categoryWithProductStoreResponse = new ArrayList<CategoryWithProductStoreResponse>();
        List<Integer> CategoriesToVerified = new ArrayList<Integer>();

        for(StoreProfileXProductResponse storeProfileXProductR : storeProfileXProductResponse){
            CategoryWithProductStoreResponse categoryWithProductStoreResponseFor;
            ProductDto_CategoryWithProductStoreResponse productDtoCategoryWithProductStoreResponseFor;
            System.out.println(storeProfileXProductR);
            if(CategoriesToVerified.size()>0){
                Integer flag = 0;
                Integer idCategoryRepeated=0;
                for(Integer CategoriesToVerifiedR: CategoriesToVerified){
                    if(storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getIdProductCategory() == CategoriesToVerifiedR){
                        flag=1;
                        idCategoryRepeated=storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getIdProductCategory();
                    }
                }

                if(flag==1){
                    for(CategoryWithProductStoreResponse categoryWithProductStoreResponseR : categoryWithProductStoreResponse){
                        if(idCategoryRepeated == categoryWithProductStoreResponseR.getIdProductCategory()){
                            ProductDto_CategoryWithProductStoreResponse pdtoCategory;
                            pdtoCategory = ProductDto_CategoryWithProductStoreResponse.builder().
                            idStoreXProduct(storeProfileXProductR.getIdStoreXProduct()).
                            idProduct(storeProfileXProductR.getProduct().getIdProduct()).
                            price(storeProfileXProductR.getPrice()).
                            content(storeProfileXProductR.getProduct().getContent()).
                            name(storeProfileXProductR.getProduct().getName()).
                            status(storeProfileXProductR.getProduct().getStatus()).
                            image(storeProfileXProductR.getProduct().getImage()).
                            brand(storeProfileXProductR.getProduct().getBrand().getName()).
                            packaging(storeProfileXProductR.getProduct().getPackaging()).
                            description(storeProfileXProductR.getProduct().getDescription()).
                            build();
                            categoryWithProductStoreResponseR.getProduct().add(pdtoCategory);
                        }
                    }
                    
                }
                else{
                    List<ProductDto_CategoryWithProductStoreResponse> productDtoCategoryWithProductStoreResponseForElse = new ArrayList<ProductDto_CategoryWithProductStoreResponse>();
                    CategoriesToVerified.add(storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getIdProductCategory());
    
                    productDtoCategoryWithProductStoreResponseFor = ProductDto_CategoryWithProductStoreResponse.builder().
                    idStoreXProduct(storeProfileXProductR.getIdStoreXProduct()).
                    idProduct(storeProfileXProductR.getProduct().getIdProduct()).
                    price(storeProfileXProductR.getPrice()).
                    content(storeProfileXProductR.getProduct().getContent()).
                    name(storeProfileXProductR.getProduct().getName()).
                    status(storeProfileXProductR.getProduct().getStatus()).
                    image(storeProfileXProductR.getProduct().getImage()).
                    brand(storeProfileXProductR.getProduct().getBrand().getName()).
                    packaging(storeProfileXProductR.getProduct().getPackaging()).
                    description(storeProfileXProductR.getProduct().getDescription()).
                    build();

                    productDtoCategoryWithProductStoreResponseForElse.add(productDtoCategoryWithProductStoreResponseFor);


                    categoryWithProductStoreResponseFor = CategoryWithProductStoreResponse.builder().
                    idProductCategory(storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getIdProductCategory()).
                    name(storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getName())
                    .ranking(storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getRanking())
                    .product(productDtoCategoryWithProductStoreResponseForElse)
                    .build();

                    categoryWithProductStoreResponse.add(categoryWithProductStoreResponseFor);

                }

            }
            else{

                List<ProductDto_CategoryWithProductStoreResponse> productDtoCategoryWithProductStoreResponseForElse = new ArrayList<ProductDto_CategoryWithProductStoreResponse>();
                CategoriesToVerified.add(storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getIdProductCategory());

                productDtoCategoryWithProductStoreResponseFor = ProductDto_CategoryWithProductStoreResponse.builder().
                idStoreXProduct(storeProfileXProductR.getIdStoreXProduct()).
                idProduct(storeProfileXProductR.getProduct().getIdProduct()).
                price(storeProfileXProductR.getPrice()).
                content(storeProfileXProductR.getProduct().getContent()).
                name(storeProfileXProductR.getProduct().getName()).
                status(storeProfileXProductR.getProduct().getStatus()).
                image(storeProfileXProductR.getProduct().getImage()).
                brand(storeProfileXProductR.getProduct().getBrand().getName()).
                packaging(storeProfileXProductR.getProduct().getPackaging()).
                description(storeProfileXProductR.getProduct().getDescription()).
                build();

                productDtoCategoryWithProductStoreResponseForElse.add(productDtoCategoryWithProductStoreResponseFor);

                categoryWithProductStoreResponseFor = CategoryWithProductStoreResponse.builder().
                idProductCategory(storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getIdProductCategory()).
                name(storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getName())
                .ranking(storeProfileXProductR.getProduct().getProductSubCategory().getProductCategory().getRanking())
                .product(productDtoCategoryWithProductStoreResponseForElse)
                .build();

                categoryWithProductStoreResponse.add(categoryWithProductStoreResponseFor);

            }

        }



        return categoryWithProductStoreResponse;


    }

}
