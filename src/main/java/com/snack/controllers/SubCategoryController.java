package com.snack.controllers;

import java.util.List;

import com.snack.dto.ProductDto;
import com.snack.dto.SubCategoryDto;
import com.snack.services.SubCategoryService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SubCategoryController {

    @Autowired
    private SubCategoryService subcategoryservice;

    @GetMapping(value = "/api/customer/subcategories/{idProductCategory}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Se obtiene una lista de subcategorías por un idCategory",
            notes = "Se especifica una categoría de la cual traer todas las categorías dependientes de esta",
            responseContainer = "Object")
    public List<SubCategoryDto> getSubcategoryByCategoryId(@PathVariable("idProductCategory") Integer idProductCategory) {
        return subcategoryservice.getSubcategoryByCategoryId(idProductCategory);
    }

}