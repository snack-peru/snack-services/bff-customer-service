package com.snack.controllers.mongo;

import com.snack.dto.mongo.BasketDTO;
import com.snack.dto.mongo.request.AddProductRequest;
import com.snack.dto.mongo.request.AssignProductsToStoreRequest;
import com.snack.dto.mongo.request.ListStoresRequest;
import com.snack.dto.mongo.request.ProductstoExclude;
import com.snack.dto.mongo.response.NearestStoresResponse;
import com.snack.dto.mongo.response.NearestStoresResponseElement;
import com.snack.services.mongo.BasketService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/customer/basket")
public class BasketController {
    
    @Autowired
    private BasketService basketService;

    //Author:Daniel
    @GetMapping(value = "/{customerId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "obtener la canasta del cliente",
            notes = "Se pasa como parámetro el customerId",
            responseContainer = "Object",
            response = BasketDTO.class)
    public BasketDTO getBasket(@PathVariable(name="customerId") Long customerId) {
        return basketService.getBasket(customerId);
    }

    //Author: Sergio
    @PostMapping(value = "/{customerId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "añadir un nuevo producto a la canasta",
            notes = "Se pasa como parámetro el customerId",
            responseContainer = "Object",
            response = void.class)
    public void newProduct(@PathVariable(name = "customerId") Long customerId, @RequestBody AddProductRequest addProductRequest) {
        basketService.newProduct(customerId, addProductRequest);
    }

    //Author: Fabricio
    @PutMapping (value =  "/{customerId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Borrar los productos de la cesta de un cliente",
            notes = "Se debe enviar como parametro el customerId y la lista de id de los productos a eliminar",
            responseContainer = "Object",
            response = Void.class)
    public void deleteProductBasket(@PathVariable(name = "customerId") Long customerId, @RequestBody ProductstoExclude productsDelete) {
        basketService.deleteProductsofBasket(customerId, productsDelete);
    }

    //Author: Martin
    @PostMapping(value = "/listStores")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Listar las tiendas cercanas a un cliente con el detalle de los productos de la canasta",
            notes = "Se debe enviar como parametro el idCustomer, products (los de la cesta), deliveryType, distancia (radio), latitud y longitud, page y pageLen",
            responseContainer = "Object",
            response = NearestStoresResponse.class)
    public List<NearestStoresResponseElement> listNearestStores(@RequestBody ListStoresRequest request) {
        System.out.println(request);
        return basketService.listStores(request);
    }

    // Author: Sergio
    @PostMapping(value = "/{customerId}/assignProductsToStore")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Asignar una lista de productos de la canasta a una tienda",
            notes = "Se debe enciar el customerId en la uri, el deliveryType como RequestParam y la lista de productos " +
                    "y el id de la tienda en el requestBody",
            responseContainer = "Object",
            response = void.class)
    public ResponseEntity<?> assignProductsToStore(@PathVariable(name = "customerId") Long customerId,
                                                @RequestParam("deliveryType") String deliveryType,
                                                @RequestBody AssignProductsToStoreRequest request) {
        try {
            basketService.assignProductsToStore(customerId, deliveryType, request);
            return ResponseEntity.status(HttpStatus.OK).body(Collections.singletonMap("message", "Producto(s) asignados a la tienda " + request.getStoreName()));
        } catch (HttpClientErrorException e) {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("message", "Hubo un error en la consulta.");
            errorMap.put("frontendMessage", "No se pudo escoger la tienda.");
            errorMap.put("status", e.getStatusCode().toString());
            errorMap.put("error", e.getMessage());
            return ResponseEntity.status(e.getStatusCode()).body(errorMap);
        }
    }
}