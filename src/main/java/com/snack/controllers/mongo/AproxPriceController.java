package com.snack.controllers.mongo;

import java.util.List;

import com.snack.dto.mongo.AproxPriceDTO;
import com.snack.dto.mongo.BasketDTO;
import com.snack.dto.mongo.ProductDTO;
import com.snack.services.mongo.AproxPriceService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/customer")
public class AproxPriceController {

    @Autowired
    private AproxPriceService aproxPriceService;

    // Author:Daniel
    @GetMapping(value = "/aproxPrice/{productId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "obtener precio aproximado de un producto",
            notes = "Se pasa como parámetro el productId",
            responseContainer = "Object",
            response = AproxPriceDTO.class)
    public AproxPriceDTO getAproxPriceByProductId(@PathVariable(name = "productId") Long productId) {
        return aproxPriceService.getAproxPriceByProductId(productId);
    }

    // Author:Daniel
    @PostMapping(value = "/ListAproxPrice/")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "obtener precio aproximado de una lista de productos",
            notes = "Se pasa un requestBody de List<String> con los ids",
            responseContainer = "Object")
    public List<AproxPriceDTO> getAproxPriceByListOfProducts(@RequestBody List<ProductDTO> idProductList) {
        return aproxPriceService.getAproxPriceByListOfProducts(idProductList);
    }

    // Author:Daniel
    @PostMapping(value = "/aproxPrice/")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Guardar precio aproximado de un producto",
            notes = "Se pasa un objeto de productId y aproxPrice",
            responseContainer = "Object",
            response = AproxPriceDTO.class)
    public AproxPriceDTO saveAproxPrice(@RequestBody AproxPriceDTO aproxPriceDTO) {
        return aproxPriceService.saveAproxPrice(aproxPriceDTO);
    }

}