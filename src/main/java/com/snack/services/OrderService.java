package com.snack.services;

import com.snack.dto.OrderDto;
import com.snack.dto.RegisterTotalOrderDto;


import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


@Service
public class OrderService {
    
    @Autowired
    private RestTemplate restTemplate;
    
    private final String ordersUri = "http://orders-service";

    public List<OrderDto> getAllOrdersByIdCustomerIdOrderStatus( Long idOrder,  Integer idOrderStatus) {

         ParameterizedTypeReference<List<OrderDto>> responseType = new ParameterizedTypeReference<List<OrderDto>>() {};
         ResponseEntity<List<OrderDto>> resp = restTemplate.exchange(ordersUri + "/api/order/" + idOrder + "/" + idOrderStatus, HttpMethod.GET, null, responseType);
         List<OrderDto> orders = resp.getBody();

        return orders;

    }

    public RegisterTotalOrderDto saveTotalOrder(Long idCustomer, RegisterTotalOrderDto registerTotalOrderDto)throws HttpClientErrorException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<RegisterTotalOrderDto> entity = new HttpEntity<RegisterTotalOrderDto>(registerTotalOrderDto, headers);

        return restTemplate.postForObject(ordersUri + "/api/order/" + idCustomer , entity, RegisterTotalOrderDto.class);

    }

}