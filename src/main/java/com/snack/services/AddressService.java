package com.snack.services;

import com.snack.dto.CategoryDto;
import com.snack.dto.request.AddressRequest;
import com.snack.dto.response.AddressResponse;
import com.snack.dto.response.GetAllAddressResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class AddressService {

    @Autowired
    private RestTemplate restTemplate;

    private final String usersUri = "http://users-service";

    // Author: Sergio
    public List<AddressResponse> getAllAddressByCustomerId(Long customerId) {

        ParameterizedTypeReference<List<AddressResponse>> responseType = new ParameterizedTypeReference<List<AddressResponse>>() {};
        ResponseEntity<List<AddressResponse>> resp =
                restTemplate.exchange(usersUri + "/api/address/" + customerId, HttpMethod.GET, null, responseType);

        List<AddressResponse> addressResponseList = resp.getBody();

        return addressResponseList;
    }

    // Author: Sergio
    public AddressResponse getDefaultAddressByCustomerId(Long customerId) {
        return restTemplate.getForObject(usersUri + "/api/address/default/" + customerId, AddressResponse.class);
    }

    // Author: Sergio
    public AddressResponse saveCustomerAddress(Long customerId, AddressRequest addressRequest) {
        return restTemplate.postForObject(usersUri + "/api/address/" + customerId, addressRequest, AddressResponse.class);
    }

    // Author: Sergio
    public void deleteCustomerAddress(Long addressId) {
        restTemplate.delete(usersUri + "/api/address/" + addressId);
    }

    // Author: Sergio
    public LongResponse updateDefaultAddress(Long customerId, Long newDefaultAddressId) {

        ResponseEntity<LongResponse> res =
                restTemplate.exchange(usersUri + "/api/address/" + customerId + "/" + newDefaultAddressId,
                HttpMethod.PUT, null, LongResponse.class);

        return res.getBody();
    }
}
