package com.snack.services;

import com.snack.dto.*;

import com.snack.dto.request.GetStoresInMapRequest;
import com.snack.dto.response.GetStoresInMapResponse;
import com.snack.dto.response.StoreResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    @Autowired
    private RestTemplate restTemplate;
    
    private final String usersUri = "http://users-service";
    private final String companyUri = "http://company-service";

    public CustomerDto getCustomerById(Long customerId) {
        // RestTemplate restTemplate = new RestTemplate();

        //para pasar un objecto por post request
        // HttpHeaders headers = new HttpHeaders();
        // headers.setContentType(MediaType.APPLICATION_JSON);
        // HttpEntity<CreditTransactionDTO> creditTrasaction = new HttpEntity<CreditTransactionDTO>(t, headers);

        CustomerDto customerDto = restTemplate.getForObject(usersUri + "/api/customer/" + customerId, CustomerDto.class);

        return customerDto;

    }

    public CustomerLogged loginCustomer(LoginDto loginDto) throws HttpClientErrorException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LoginDto> entity = new HttpEntity<LoginDto>(loginDto, headers);

        return restTemplate.postForObject(usersUri + "/api/customer/login", entity, CustomerLogged.class);
        
    }

    public CustomerLogged registerCustomer(RegisterUser user) throws HttpClientErrorException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<RegisterUser> entity = new HttpEntity<RegisterUser>(user, headers);

        return restTemplate.postForObject(usersUri + "/api/customer/register", entity, CustomerLogged.class);
    }

    public UserDto updateCustomer(UserDto user) throws HttpClientErrorException {
        // Get userId
        CustomerDto customerDetail = getCustomerById(user.getIdUser());
        // set userId for update
        user.setIdUser(customerDetail.getUser().getIdUser());

        // Call request for update
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<UserDto> entity = new HttpEntity<UserDto>(user, headers);

        return restTemplate.postForObject(usersUri + "/api/user/update", entity, UserDto.class);
    }

    public List<GetStoresInMapResponse> getStoresInMap(Double latitude, Double longitude, GetStoresInMapRequest request,
                                                       Double radiusDistance) {
        /* DESCRIPCIÓN:
        * Obtener las tiendas cercanas a tu ubicación en el mapa, pasando un objeto con una lista de storesIds
        * para que no se regresen tiendas repetidas */

        // llamar al api para obtener las tiendas cercanas
        List<StoreResponse> stores = Arrays.asList(restTemplate.getForObject( companyUri+ "/api/store/"
                + latitude + "/" + longitude + "/" + radiusDistance, StoreResponse[].class));

        // filtrar por tiendas que ya se tienen en la variable request
        return stores.stream()
                .filter(store -> !request.getStoreIdList().contains(store.getIdStore()))
                .map(store -> {
                    return GetStoresInMapResponse.builder()
                            .storeId(store.getIdStore())
                            .title(store.getName())
                            .description(store.getAddress())
                            .image(store.getImage())
                            .distance(this.getDistanceBetweenCoordinates(latitude, longitude, store.getLatitude(), store.getLongitude()))
                            .coordinates(Coordinates.builder().latitude(store.getLatitude()).longitude(store.getLongitude()).build())
                            .rating(store.getScore())
                            .reviews(store.getReviews())
                            .status(store.getStatusStore().getIdStatusStore() == 1 ? "Abierto" : "Cerrado")
                            .deliveryType(store.getDeliveryType().getName())
                            .build();
                })
                .sorted(Comparator.comparing(GetStoresInMapResponse::getDistance))
                .collect(Collectors.toList());
    }

    private Double getDistanceBetweenCoordinates(Double x1, Double y1, Double x2, Double y2) {
        return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
    }
}