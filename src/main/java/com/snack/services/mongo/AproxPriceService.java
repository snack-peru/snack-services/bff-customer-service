package com.snack.services.mongo;

import java.util.List;

import com.snack.dto.mongo.AproxPriceDTO;
import com.snack.dto.mongo.ProductDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AproxPriceService {
    
    @Autowired
    private RestTemplate restTemplate;

    private final String productsUri = "http://products-service";

    public AproxPriceDTO getAproxPriceByProductId(Long productId) {

        ParameterizedTypeReference<AproxPriceDTO> responseType = new ParameterizedTypeReference<AproxPriceDTO>() {};
        ResponseEntity<AproxPriceDTO> resp = restTemplate.exchange(productsUri + "/api/aproxPrice/"+productId, HttpMethod.GET, null, responseType);
        AproxPriceDTO aproxPriceDTO = resp.getBody();

        return aproxPriceDTO;
    }

    public List<AproxPriceDTO> getAproxPriceByListOfProducts(List<ProductDTO> idProductList) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<ProductDTO>> entity = new HttpEntity<List<ProductDTO>>(idProductList, headers);

        ParameterizedTypeReference<List<AproxPriceDTO>> responseType = new ParameterizedTypeReference <List<AproxPriceDTO>>() {};
        ResponseEntity<List<AproxPriceDTO>> resp = restTemplate.exchange(productsUri + "/api/ListAproxPrice/", HttpMethod.POST, entity, responseType);
        List<AproxPriceDTO> aproxPriceDTOlist = resp.getBody();

        return aproxPriceDTOlist;

    }

    public AproxPriceDTO saveAproxPrice(AproxPriceDTO aproxPriceDTO) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AproxPriceDTO> entity = new HttpEntity<AproxPriceDTO>(aproxPriceDTO, headers);
        return restTemplate.postForObject(productsUri + "/api/aproxPrice/", entity, AproxPriceDTO.class);
    }
}