package com.snack.services.mongo;

import com.snack.dto.StoreXProductDTO;
import com.snack.dto.mongo.AproxPriceDTO;
import com.snack.dto.mongo.BasketDTO;
import com.snack.dto.mongo.ProductDTO;
import com.snack.dto.mongo.request.*;

import com.snack.dto.mongo.response.NearestStoresResponseElement;
import com.snack.dto.mongo.response.ProductAvailability;
import com.snack.services.StoreXProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BasketService {
    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private StoreXProductService storeXProductService;

    @Autowired
    private AproxPriceService aproxPriceService;

    private final String ordersUri = "http://orders-service";
    private final String companyUri = "http://company-service";

    //Daniel
    public BasketDTO getBasket(Long customerId) {

        // Se obtiene la canasta del cliente
        ParameterizedTypeReference<BasketDTO> responseType = new ParameterizedTypeReference<BasketDTO>() {};
        ResponseEntity<BasketDTO> resp = restTemplate.exchange(ordersUri + "/api/basket/"+customerId, HttpMethod.GET, null, responseType);
        BasketDTO basketDto = resp.getBody();

        // Obtener los precios aproximados de los productos sin tienda asignada
        if(basketDto.getProductsAlone() != null){
            List<AproxPriceDTO> aproxPriceList = aproxPriceService.getAproxPriceByListOfProducts(basketDto.getProductsAlone());
            Map<Long, Double> aproxPriceMap = new HashMap<>();
            for (AproxPriceDTO aproxPriceDTO : aproxPriceList) {
                aproxPriceMap.put(aproxPriceDTO.getProductId(), aproxPriceDTO.getAproxPrice());
            }

            for (ProductDTO productDto : basketDto.getProductsAlone()) {
                productDto.setPrice(aproxPriceMap.get(productDto.getProductId()));
            }
        }

        if(basketDto.getStores() != null){
            basketDto.setStores(storeXProductService.getStoresWithPriceProducts(basketDto.getStores()));
        }

        return basketDto;
    }

    // Author: Sergio
    public void newProduct(Long customerId, AddProductRequest addProductRequest) {
        restTemplate.postForObject(ordersUri + "/api/basket/" + customerId, addProductRequest, String.class);
    }

    // Author: Fabricio
    public void deleteProductsofBasket(Long customerId, ProductstoExclude productsDelete){
        restTemplate.put(ordersUri + "/api/basket/" + customerId, productsDelete);
    }


    //Author: Martin
    private List<NearestStoresResponseElement> getNearestStores(double latitude, double longitude, double distance) throws HttpClientErrorException {
        try {
            ParameterizedTypeReference<List<NearestStoresResponseElement>> responseType = new ParameterizedTypeReference<List<NearestStoresResponseElement>>() {};
            ResponseEntity<List<NearestStoresResponseElement>> response = restTemplate
                    .exchange(companyUri + "/api/store/" + String.valueOf(latitude) + "/"
                            + String.valueOf(longitude) + "/" + String.valueOf(distance), HttpMethod.GET, null, responseType);
            List<NearestStoresResponseElement> nearestStoresList = new ArrayList<NearestStoresResponseElement>();
            for(NearestStoresResponseElement store : response.getBody()){
                if(store.getStatusStore().getIdStatusStore() == 1)
                    nearestStoresList.add(store);
            }
            return nearestStoresList;
        } catch (HttpClientErrorException e) {
            System.out.println("ERROR ON GET DATA FROM COMPANY-SERVICE");
            return new ArrayList<NearestStoresResponseElement>();
        }
    }

    //Author: Martin
    public List<NearestStoresResponseElement> listStores(ListStoresRequest request) {
        /*
        List<NearestStoresResponseElement> nearestStores = getNearestStores(request.getLatitude(), request.getLongitude(), request.getDistance());
        for (int i = 0; i < nearestStores.size(); i++) {
            List<ProductDto> availableProducts = productService.getProductsByStore(nearestStores.get(i).getIdStore());
            for(BasketItem b : request.getProducts()) {
                Optional<ProductDto> productDetail = availableProducts.stream().filter(prod -> prod.getIdProduct() == b.getId()).findAny();
                if (productDetail.isPresent()) {
                    ProductAvailability availableProduct = new ProductAvailability();
                    availableProduct.setIdProduct(productDetail.get().getIdProduct());
                    availableProduct.setAvailable(true);
                    nearestStores.get(i).addProduct(availableProduct);
                }
            }
        }
        System.out.println(nearestStores);
        return nearestStores;
        */
        List<NearestStoresResponseElement> nearestStores = getNearestStores(request.getLatitude(), request.getLongitude(), request.getDistance());

        for (int i = 0; i < nearestStores.size(); i++) {
            List<StoreXProductDTO> lsStoreXProductDTO = storeXProductService.getProductsByIdStore(nearestStores.get(i).getIdStore());
            for(BasketItem basketItem : request.getProducts()) {
                for(StoreXProductDTO storeXProductDTO : lsStoreXProductDTO) {
                    if(basketItem.getId().equals(storeXProductDTO.getProduct().getIdProduct())) {
                        ProductAvailability availableProduct = new ProductAvailability();
                        availableProduct.setIdProduct(storeXProductDTO.getProduct().getIdProduct());
                        availableProduct.setAvailable(true);
                        availableProduct.setPrice(storeXProductDTO.getPrice());
                        nearestStores.get(i).addProduct(availableProduct);
                    }
                }
            }
        }

        List<NearestStoresResponseElement> nearestStoresWithProducts = new ArrayList<NearestStoresResponseElement>();
        for(NearestStoresResponseElement store: nearestStores){
            if(store.getProducts().size() > 0)
                nearestStoresWithProducts.add(store);
        }

        return nearestStoresWithProducts;
    }

    public void assignProductsToStore(Long customerId, String deliveryType, AssignProductsToStoreRequest request) {
        restTemplate.postForObject(ordersUri + "/api/basket/" + customerId + "/assignProductsToStore?deliveryType=" + deliveryType,
                request, void.class);
    }
}