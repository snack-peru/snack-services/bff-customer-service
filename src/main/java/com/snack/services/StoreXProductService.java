package com.snack.services;

import java.util.List;
import com.snack.dto.StoreXProductDTO;
import com.snack.dto.mongo.StoreDTO;
import com.snack.dto.response.StoreProfileXProductResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service

public class StoreXProductService {

    @Autowired
    private RestTemplate restTemplate;

    private final String productsUri = "http://products-service";

    public List<StoreProfileXProductResponse> getProfileStoreProducts(Integer idStore) {
        ParameterizedTypeReference<List<StoreProfileXProductResponse>> responseType = new ParameterizedTypeReference<List<StoreProfileXProductResponse>>() {};
        ResponseEntity<List<StoreProfileXProductResponse>> resp = restTemplate.exchange(productsUri + "/api/storexproduct/store/" + idStore, HttpMethod.GET, null, responseType);
        List<StoreProfileXProductResponse> result = resp.getBody();
        return result;
    }

    public List<StoreXProductDTO> getProductsByIdStore(Integer idStore) {
        ParameterizedTypeReference<List<StoreXProductDTO>> responseType = new ParameterizedTypeReference<List<StoreXProductDTO>>() {};
        ResponseEntity<List<StoreXProductDTO>> resp = restTemplate.exchange(productsUri + "/api/storexproduct/store/" + idStore + "/products", HttpMethod.GET, null, responseType);
        List<StoreXProductDTO> result = resp.getBody();
        return result;
    }

    public List<StoreDTO> getStoresWithPriceProducts(List<StoreDTO> storeAndProductRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<StoreDTO>> entity = new HttpEntity<List<StoreDTO>>(storeAndProductRequest, headers);

        ParameterizedTypeReference<List<StoreDTO>> responseType = new ParameterizedTypeReference <List<StoreDTO>>() {};

        ResponseEntity<List<StoreDTO>> response = restTemplate.exchange(productsUri + "/api/storexproduct/store/products/realPrice",HttpMethod.POST, entity, responseType);

        List<StoreDTO> storeAndProductResponse = response.getBody();

        return storeAndProductResponse;

    }
    
}
