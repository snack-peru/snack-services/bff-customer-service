package com.snack.services;

import java.util.List;

import com.snack.dto.CategoryDto;
import com.snack.dto.response.SubcategoryResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CategoryService {

    @Autowired
    private RestTemplate restTemplate;

    private final String productsUri = "http://products-service";

    public List<CategoryDto> getAllProductsCategories() {

        //Estas 3 lineas que siguen es un truco para poder recibir un List<Entity> como respuesta
        ParameterizedTypeReference<List<CategoryDto>> responseType = new ParameterizedTypeReference<List<CategoryDto>>() {};
        ResponseEntity<List<CategoryDto>> resp = restTemplate.exchange(productsUri + "/api/categories", HttpMethod.GET, null, responseType);
        List<CategoryDto> categories = resp.getBody();

        return categories;
    }

    public List<CategoryDto> getCategoriesByRanking() {

        ParameterizedTypeReference<List<CategoryDto>> responseType = new ParameterizedTypeReference<List<CategoryDto>>() {};
        ResponseEntity<List<CategoryDto>> resp = restTemplate.exchange(productsUri + "/api/categoriesByRanking", HttpMethod.GET, null, responseType);
        List<CategoryDto> categories = resp.getBody();

        return categories;
    }

    public Resource getCategoryImage(String name) {
        return restTemplate.getForObject(productsUri + "/api/category/image/" + name, Resource.class);
    }

    public List<SubcategoryResponse> getSubcategoriesWithProductsByCategory(Integer categoryId) {
        ParameterizedTypeReference<List<SubcategoryResponse>> responseType = new ParameterizedTypeReference<List<SubcategoryResponse>>() {};
        ResponseEntity<List<SubcategoryResponse>> resp = restTemplate.exchange(productsUri + "/api/category/" + categoryId + "/subcategories", HttpMethod.GET, null, responseType);
        List<SubcategoryResponse> result = resp.getBody();
        return result;
    }
}