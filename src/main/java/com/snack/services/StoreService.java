package com.snack.services;

import java.util.List;

import com.snack.dto.PaymentTypeDto;
import com.snack.dto.response.StoreResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class StoreService {
    
    @Autowired
    private RestTemplate restTemplate;

    private final String storesUri = "http://company-service";

    public List<StoreResponse> getStoresByName(String searchkey) {
        ParameterizedTypeReference<List<StoreResponse>> responseType = new ParameterizedTypeReference<List<StoreResponse>>() {};
        ResponseEntity<List<StoreResponse>> resp = restTemplate.exchange(storesUri + "/api/store/search/" + searchkey, HttpMethod.GET, null, responseType);
        List<StoreResponse> result = resp.getBody();
        return result;
    }

    public List<PaymentTypeDto> getPaymentTypes(Long idStore) {
        ParameterizedTypeReference<List<PaymentTypeDto>> responseType = new ParameterizedTypeReference<List<PaymentTypeDto>>() {};
        ResponseEntity<List<PaymentTypeDto>> response = restTemplate.exchange(storesUri + "/api/paymentTypeXStore/" + idStore.toString(), HttpMethod.GET, null, responseType);
        List<PaymentTypeDto> result = response.getBody();
        return result;
    }
}
