package com.snack.services;

import java.util.List;

import com.snack.dto.SubCategoryDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SubCategoryService {

    @Autowired
    private RestTemplate restTemplate;

    private final String productsUri = "http://products-service";

    public List<SubCategoryDto> 
    getSubcategoryByCategoryId(Integer idProductCategory) {
        
        ParameterizedTypeReference<List<SubCategoryDto>> responseType = new ParameterizedTypeReference<List<SubCategoryDto>>() {};
        ResponseEntity<List<SubCategoryDto>> resp = restTemplate.exchange(productsUri + "/api/subcategories/" + idProductCategory, HttpMethod.GET, null, responseType);
        List<SubCategoryDto> subcategories = resp.getBody();

        return subcategories;
    }
}