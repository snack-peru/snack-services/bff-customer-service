package com.snack.services;

import java.util.ArrayList;
import java.util.List;

import com.snack.dto.ProductDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.io.Resource;

@Service
public class ProductService {

    @Autowired
    private RestTemplate restTemplate;

    private final String productsUri = "http://products-service";

    public List<ProductDto> getNewProducts() {
        ParameterizedTypeReference<List<ProductDto>> responseType = new ParameterizedTypeReference<List<ProductDto>>() {};
        ResponseEntity<List<ProductDto>> resp = restTemplate.exchange(productsUri + "/api/product/newproducts", HttpMethod.GET, null, responseType);
        List<ProductDto> result = resp.getBody();
        return result;
    }

    public ProductDto getProductById(Long idProduct) {
        ParameterizedTypeReference<ProductDto> responseType = new ParameterizedTypeReference<ProductDto>() {};
        ResponseEntity<ProductDto> resp = restTemplate.exchange(productsUri + "/api/product/" + idProduct, HttpMethod.GET, null, responseType);
        ProductDto result = resp.getBody();
        return result;
    }

    public List<ProductDto> getProductByName(String searchkey) {
        ParameterizedTypeReference<List<ProductDto>> responseType = new ParameterizedTypeReference<List<ProductDto>>() {};
        ResponseEntity<List<ProductDto>> resp = restTemplate.exchange(productsUri + "/api/product/search/" + searchkey, HttpMethod.GET, null, responseType);
        List<ProductDto> result = resp.getBody();
        return result;
    }

    public Resource getProductImage(String name) {
        return restTemplate.getForObject(productsUri + "/api/product/image/" + name, Resource.class);
    }

    public List<ProductDto> getProductsByCategory(Integer categoryId) {
        ParameterizedTypeReference<List<ProductDto>> responseType = new ParameterizedTypeReference<List<ProductDto>>() {};
        ResponseEntity<List<ProductDto>> resp = restTemplate.exchange(productsUri + "/api/productsbycategory/" + categoryId, HttpMethod.GET, null, responseType);
        List<ProductDto> result = resp.getBody();
        return result;
    }

    //Author: Martin
    public List<ProductDto> getProductsByStore(Long idStore) throws HttpClientErrorException {
        try {
            ParameterizedTypeReference<List<ProductDto>> responseType = new ParameterizedTypeReference<List<ProductDto>>() {};
            String productByStoreUrl = productsUri + "/api/productByStore/"+ idStore;
            ResponseEntity<List<ProductDto>> response = restTemplate.exchange(productByStoreUrl, HttpMethod.GET, null, responseType);
            List<ProductDto> productsResponse = response.getBody();
            return productsResponse;
        } catch (HttpClientErrorException e) {
            System.out.println("ERROR ON GET DATA FROM COMPANY-SERVICE");
            return new ArrayList<ProductDto>();
        }
    }
}

